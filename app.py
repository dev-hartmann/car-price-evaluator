import settings
import ml.model as model

from flask import Flask, request, jsonify
from webargs import fields, validate
from webargs.flaskparser import parser, use_args

app = Flask(__name__)

data_point_args = {
    'price': fields.Int(required=True),
    'age': fields.Int(required=True),
    'km': fields.Int(required=True),
    'fueltype': fields.Str(required=True),
    'hp': fields.Int(required=True),
    'metcolor': fields.Int(required=True),
    'automatic': fields.Int(required=True),
    'cc': fields.Int(required=True),
    'doors': fields.Int(required=True),
    'weight': fields.Int(required=True),
}


@app.route('/predict', methods=['GET'])
@use_args(data_point_args)
def predict(args):

    data  = {'Price': args.get('price'),
    'Age': args.get('age'),
    'KM': args.get('km'),
    'FuelType':  args.get('fueltype'),
    'HP': args.get('hp'),
    'MetColor': args.get('metcolor'),
    'Automatic': args.get('automatic'),
    'CC': args.get('cc'),
    'Doors': args.get('doors'),
    'Weight': args.get('weight')}

    result = model.evaluate_price(data)
    response = jsonify({'Result': result})
    response.status_code = 201
    return response


@app.errorhandler(422)
def validation_error_handler(err):
    exc = err.exc
    return jsonify({'errors': exc.messages})


def initialize_app(flask_app):
    flask_app.config['SERVER_NAME'] = settings.FLASK_SERVER_NAME
    model.train_linear_model()


def main():
    initialize_app(app)
    app.run(debug=settings.FLASK_DEBUG)


if __name__ == "__main__":
    main()
