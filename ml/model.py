import settings
import pandas as pd
from sklearn import linear_model, metrics, model_selection
import numpy as np

model = None
model_score = None
r2_score = None
MSE = None

def train_linear_model():
    # read in data from csv
    df = pd.read_csv(settings.MODELL_DATA_PATH)
    # clean dataset -> create numerical value vor categorial data (FuelType)
    df['TypeOfFuel'] = df['FuelType'].map({'Diesel': 1, 'Petrol': 2, 'CNG': 3}).astype(int)

    # drop FuelType column
    df.drop('FuelType', axis=1, inplace=True)

    # Create feature-set X and target y
    X = df.drop('Price', axis=1)
    y = df['Price']

    # split data in training and test data
    X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.20, random_state=5)

    # create and fit model
    linear_mod = linear_model.LinearRegression()
    linear_mod.fit(X_train, y_train)

    predicted_train = linear_mod.predict(X_train)
    predicted_test = linear_mod.predict(X_test)
    global model_score
    model_score = linear_mod.score(X_test, y_test)
    global model
    model = linear_mod


def percentage(percent, whole):
  return (percent * whole) / 100.0


def get_model_r2():
    if model is None or model_score is None:
        train_linear_model()
    else:
        return model_score


def predict_price(car_data):
    new_data = [car_data]

    # cloumns[0].keys() call is needed, because otherwise column key order is not presever leading to wonky predictions
    to_predict = pd.DataFrame(new_data, columns=new_data[0].keys())

    # transform categorical data
    to_predict['TypeOfFuel'] = to_predict['FuelType'].map({'Diesel': 1, 'Petrol': 2, 'CNG': 3}).astype(int)

    # drop obsolete columns
    to_predict.drop(['Price', 'FuelType'], axis=1, inplace=True)

    result = model.predict(to_predict)
    return result


def calculate_price_equality_range(predicted_price):
    error_percentage = 100 * (1 - model_score)
    error_margin = percentage((error_percentage / 2), predicted_price)
    return error_margin


def evaluate_price(car_data):
    price_to_evaluate = car_data['Price']
    predicted_price = predict_price(car_data)[0]

    error_margin = calculate_price_equality_range(predicted_price)
    lower_bound = predicted_price - error_margin
    upper_bound = predicted_price + error_margin

    if lower_bound <= price_to_evaluate <= upper_bound:
        return "Fair Price", price_to_evaluate, predicted_price
    elif price_to_evaluate < lower_bound:
        return "Bargain", price_to_evaluate, predicted_price
    elif price_to_evaluate > upper_bound:
        return "Bad Deal", price_to_evaluate, predicted_price
    else:
        raise Exception("Could not evaluate price, something went wrong")
