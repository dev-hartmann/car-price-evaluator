# Flask settings
FLASK_SERVER_NAME = 'localhost:8888'
FLASK_DEBUG = True

# Modell
MODELL_DATA_PATH = "./data/toyota_prices.csv"
